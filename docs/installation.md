# Installation


## Steam Workshop
* To install the addon you'll first have to install the Xenin Framework. This can be done by adding [this](https://steamcommunity.com/sharedfiles/filedetails/?id=1900562881) to your servers workshop collection. **This method is heavily recommeded**

## GitLab
* If you want to just install it, navigate to [releases](https://gitlab.com/sleeppyy/xeninui/-/releases) and download the newest version.
After this you can simply just drag the addon into your servers addon folder.

* If you are a developer, you will need to make use of [LAUX](https://gitlab.com/sleeppyy/laux) as this project is not written in Lua, but a superset similar to Lua.